import { useState } from "react";
import { createContainer } from "./unstated-next"

const useCounter = (props: any) => {
    const { initNum } = props;
    const [num, changeNum] = useState<number>(initNum);
    let decrement = () => changeNum(num - 1)
    let increment = () => changeNum(num + 1)
    return {num, decrement, increment};
}

export const Counter = createContainer(useCounter);