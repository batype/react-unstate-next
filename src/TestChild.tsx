import { Counter } from "./Context"

export const TestChild = () => {
    const { num, increment } = Counter.useContainer();
    return <>
        {num} <button onClick={increment}>加法</button>
    </>
}
