import './App.css'
import { Counter } from './Context'
import { TestCounter } from './TestCounter'

export default function App() {
  return (
    <Counter.Provider initialState={{ initNum: 1 }}>
      <TestCounter />
    </Counter.Provider>
  )
}