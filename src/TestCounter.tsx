import { Counter } from "./Context"
import {TestChild} from './TestChild'

export const TestCounter = () => {
    const { num, decrement } = Counter.useContainer();
    
    return <>
        <button onClick={decrement}>减法</button>
        <TestChild />
    </>
}